# NetCore6



## Getting started
chạy trên nền tảng window 

## Project chỉ cần các bước sau 
## step 1
clone source về

## step 2
- docker-compose build
- nếu chỉ cần chạy mà không tốn nhiều tài nguyên : docker-compose up

## step 3
run địa chỉ : http://localhost:5000/products


## Tôi muốn lưu các bước phát triển được dự án như hiện tại - các bạn có thể tham khảo 
## step 1
- download netcore 6
- https://dotnet.microsoft.com/download

## step 2 - cài đặt các công cụ entity framework
- dotnet tool install --global dotnet-ef
## step 3 - tạo một folder mới 
- dotnet new mvc -n "WebApiEntityFrameworkDockerSqlServer" -lang "C#" -au none

## step 4 - add các công cụ phục vụ cho công việc 
- dotnet add package Microsoft.EntityFrameworkCore --version 6.0.8
- dotnet add package Microsoft.EntityFrameworkCore.SqlServer --version 6.0.8
- dotnet add package Microsoft.EntityFrameworkCore.Design --version 6.0.8

## step 5 - add các file Dockerfile docker composer , config setting , tạo data ...



